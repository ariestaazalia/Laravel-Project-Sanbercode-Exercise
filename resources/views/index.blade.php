<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Day 1 Sanbercode</title>
        
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">
    </head>
    <body>
        <h1>SanberBook</h1>

        <h2>Social Media Developer Santai Berkualitas</h2>
        <p>Belajar dan Berbagi agar hidup ini semakin santai berkualitas</p>

        <h2>Benefit Join di SanberBook</h2>
        <ul>
            <li>Mendapatkan motivasi sesama developer</li>
            <li>Sharing knowledge dari para mastah Sanber</li>
            <li>Dibuat oleh calon web developer terbaik</li>
        </ul>

        <h2>Cara Bergabung ke SanberBook</h2>
        <ol>
            <li>Mengunjungi Website ini</li>
            <li>Mendaftar di <a href="/register">Form Sign Up</a> </li>
            <li>Selesai</li>
        </ol>
            
    </body>
</html>