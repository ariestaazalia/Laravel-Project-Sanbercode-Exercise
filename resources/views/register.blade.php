<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Sign Up Form Sanbercode</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">

    </head>
    <body>
        <div class="flex-center position-ref full-height">
            <div class="content">
                <div class="title m-b-md">
                    <h1>Buat Account Baru!</h1>
                    <h2>Sign Up Form</h2>
                    <form action="/submit" method="POST">
                        @csrf
                        <label>First name:</label><br>
                        <input type="text" name="fname"><br><br>
                
                        <label>Last name:</label><br>
                        <input type="text" name="lname"><br><br>
                
                        <label>Gender</label><br>
                        <input type="radio" name="gender" value="male">Male<br>
                        <input type="radio" name="gender" value="female">Female<br>
                        <input type="radio" name="gender" value="other">Other<br><br>
                
                        <label>Nationality:</label><br>
                        <select name="nationality">
                            <option value="ind">Indonesian</option>
                            <option value="sig">Singaporean</option>
                            <option value="mal">Malaysian</option>
                            <option value="aus">Australian</option>
                        </select><br><br>
                
                        <label>Language Spoken:</label><br>
                        <input type="checkbox" name="lang" value="bindo">Bahasa Indonesia<br>
                        <input type="checkbox" name="lang" value="eng">English<br>
                        <input type="checkbox" name="lang" value="other">Other<br><br>
                
                        <label>Bio:</label><br>
                        <textarea rows="10" cols="40"></textarea><br><br>
                
                        <input type="submit" value="Sign Up">
                
                    </form>
                </div>

                
            </div>
        </div>
    </body>
</html>
